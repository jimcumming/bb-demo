from index import Demo


def test_add():
    d = Demo
    num = d.add(2, 5)
    assert 7 == num


def test_minus():
    d = Demo
    num = d.minus(10, 5)
    assert 5 == num
